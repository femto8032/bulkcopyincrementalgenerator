using System.Text;
using BulkCopy.SourceGeneration.Options;

namespace BulkCopy.SourceGeneration.IncrementalGenerator;

internal static class CommonSourceGenerationHelper
{
    private const string BulkCopyNamespacePlaceholder = nameof(BulkCopyNamespacePlaceholder);
    private const string BulkCopyClassPlaceholder = nameof(BulkCopyClassPlaceholder);
    private const string TargetEntityPlaceholder = nameof(TargetEntityPlaceholder);
    private const string TargetEntityNamespacePlaceholder = nameof(TargetEntityNamespacePlaceholder);
    private const string TargetRows = nameof(TargetRows);
    private const string TargetClassPropertiesWithValues = nameof(TargetClassPropertiesWithValues);
    private const string TargetEntityParameterName = "item";
    
    internal const string TableMappingAttributeDeclaration = @"namespace BulkCopy;

using System;

[AttributeUsage(AttributeTargets.Class)]
public sealed class BulkCopyAttribute : Attribute
{
    public BulkCopyAttribute(Type targetEntity)
    {
        TargetEntity = targetEntity;
    }

    public Type TargetEntity { get; }
}";
    
    internal const string TableToEntityMapStructDeclaration = @"namespace BulkCopy;

using System.Collections.Generic;

public struct TableToEntityMap
{
    public string TableName { get; set; }
    
    public IReadOnlyList<PropertyToColumnMap> PropertyToColumnMap { get; set; }
}";
    
    internal const string PropertyToColumnMapStructDeclaration = @"namespace BulkCopy;

public struct PropertyToColumnMap
{
    public string PropertyName { get; set; }
    
    public string ColumnName { get; set; }
}";

    private const string BulkCopyClassTemplate = @$"using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using BulkCopy;
using Npgsql;
using {TargetEntityNamespacePlaceholder};

namespace {BulkCopyNamespacePlaceholder};

public partial class {BulkCopyClassPlaceholder}
{{
    private readonly NpgsqlConnection _npgsqlConnection;

    public {BulkCopyClassPlaceholder}(NpgsqlConnection npgsqlConnection)
    {{
        _npgsqlConnection = npgsqlConnection;
    }}

    public async Task CopyAsync(IReadOnlyList<{TargetEntityPlaceholder}> source, TableToEntityMap tableToEntityMap, CancellationToken cancellationToken = default)
    {{
        var tableColumns = string
            .Join("", "", tableToEntityMap.PropertyToColumnMap
            .Select(p => $@""""""{{p.ColumnName}}""""""));
        
        await using var writer = await _npgsqlConnection?.BeginBinaryImportAsync(
            $@""COPY """"{{tableToEntityMap.TableName}}"""" ({{tableColumns}}) FROM STDIN (FORMAT BINARY)"",
            cancellationToken)!;
        
        foreach (var item in source)
        {{
            await writer.StartRowAsync(cancellationToken);
            foreach (var propertyName in tableToEntityMap.PropertyToColumnMap.Select(p => p.PropertyName))
            {{
                await writer.WriteAsync(GetPropertyValue(propertyName, item), cancellationToken);
            }}
        }}

        await writer.CompleteAsync(cancellationToken);
    }}

    private static object GetPropertyValue(string propertyName, {TargetEntityPlaceholder} item)
    {{
        return propertyName switch
        {{
{TargetClassPropertiesWithValues}
            _ => throw new ArgumentOutOfRangeException(nameof(propertyName))
        }};
    }}
}}";

    internal static string GetFilledClassTemplate(TypeToGenerate typeToGenerate)
    {
        var readyTemplate = new StringBuilder(BulkCopyClassTemplate)
            .Replace(BulkCopyNamespacePlaceholder, typeToGenerate.BulkCopyClassData.TypeNamespace)
            .Replace(BulkCopyClassPlaceholder, typeToGenerate.BulkCopyClassData.TypeName)
            .Replace(TargetEntityNamespacePlaceholder, typeToGenerate.TargetEntityClassData.TypeNamespace)
            .Replace(TargetEntityPlaceholder, typeToGenerate.TargetEntityClassData.TypeName)
            .Replace(TargetRows, GetTargetRows(typeToGenerate.TargetEntityProperties))
            .Replace(TargetClassPropertiesWithValues, GetTargetClassProperties(typeToGenerate.TargetEntityProperties))
            .ToString();

        return readyTemplate;
    }

    private static string GetTargetRows(IReadOnlyList<string> propertyNames)
    {
        return string
            .Join(@"
", propertyNames
                .Select(p => $@"            await writer.WriteAsync({TargetEntityParameterName}.{p}, cancellationToken);"));
    }
    
    private static string GetTargetClassProperties(IReadOnlyList<string> propertyNames)
    {
        return string
            .Join(@"
", propertyNames.Select(p => $@"            ""{p}"" => item.{p},"));
    }
}