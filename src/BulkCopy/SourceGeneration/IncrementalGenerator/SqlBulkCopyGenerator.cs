using System.Diagnostics;
using System.Text;
using BulkCopy.SourceGeneration.Options;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace BulkCopy.SourceGeneration.IncrementalGenerator;

[Generator]
public sealed class SqlBulkCopyGenerator : IIncrementalGenerator
{
    private const string BulkCopyAttributeFullName = "BulkCopy.BulkCopyAttribute";

    public void Initialize(IncrementalGeneratorInitializationContext context)
    {
        //Debugger.Launch();
        
        // Создаём маркировочный атрибут
        context.RegisterPostInitializationOutput(ctx =>
        {
            ctx.AddSource("BulkCopyAttribute.g.cs", 
                SourceText.From(CommonSourceGenerationHelper.TableMappingAttributeDeclaration, Encoding.UTF8));
            
            ctx.AddSource("TableToEntityMap.g.cs", 
                SourceText.From(CommonSourceGenerationHelper.TableToEntityMapStructDeclaration, Encoding.UTF8));
            
            ctx.AddSource("PropertyToColumnMap.g.cs", 
                SourceText.From(CommonSourceGenerationHelper.PropertyToColumnMapStructDeclaration, Encoding.UTF8));
        });
        
        var typeDeclaration = context.SyntaxProvider
            .CreateSyntaxProvider(
                predicate: static (s, _) => IsSyntaxTargetForGeneration(s),
                transform: static (ctx, ct) => GetSemanticTargetForGeneration(ctx, ct)) // 
            .Where(static m => m is not null)!;
        
        context.RegisterSourceOutput(typeDeclaration,
            static (ctx, mappingOption) =>
            {
                if (mappingOption == null) 
                    return;
                
                // Генерация исходного кода из шаблона.
                var template = CommonSourceGenerationHelper.GetFilledClassTemplate(mappingOption.Value);
                ctx.AddSource($"{mappingOption.Value.BulkCopyClassData.TypeName}.g.cs", SourceText.From(template, Encoding.UTF8));
            });
    }

    /// <summary>
    /// Выбрать типы с ключевым словом Partial и атрубутами
    /// </summary>
    /// <param name="syntaxNode"></param>
    /// <returns></returns>
    private static bool IsSyntaxTargetForGeneration(SyntaxNode syntaxNode)
    {
        return syntaxNode is TypeDeclarationSyntax { AttributeLists.Count: > 0 } typeDeclarationSyntax
               && typeDeclarationSyntax.Modifiers.Any(SyntaxKind.PartialKeyword);
    }
    
    /// <summary>
    /// Получить структуру с метаданными о типе для заполнения шаблона. 
    /// </summary>
    /// <param name="ctx"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    private static TypeToGenerate? GetSemanticTargetForGeneration(GeneratorSyntaxContext ctx, CancellationToken cancellationToken)
    {
        var typeDeclarationSyntax = (TypeDeclarationSyntax)ctx.Node;

        // Поиск по всем аттрибутам
        foreach (var attributeListSyntax in typeDeclarationSyntax.AttributeLists)
        {
            foreach (var attributeSyntax in attributeListSyntax.Attributes)
            {
                cancellationToken.ThrowIfCancellationRequested();

                // Отсеиваем массивы, указатели и параметры типа
                if (ctx.SemanticModel.GetSymbolInfo(attributeSyntax, cancellationToken: cancellationToken).Symbol is not IMethodSymbol attributeSymbol)
                    continue;

                string attributeFullName = attributeSymbol.ContainingType.ToDisplayString();

                // Отсеиваем все атрибуты кроме маркировочного
                if (attributeFullName 
                    != BulkCopyAttributeFullName 
                    || ctx.SemanticModel.GetDeclaredSymbol(typeDeclarationSyntax, cancellationToken: cancellationToken) 
                        is not { } bulkCopyTypeDeclaration)
                    continue;
                
                var attributes = bulkCopyTypeDeclaration.GetAttributes();
                
                // Искомый аттрибут
                var bulkCopyAttribute = attributes
                    .Single(a => a.AttributeClass?.ToDisplayString() == BulkCopyAttributeFullName);
                
                // Получаем данные о типе, который будет копироваться в БД
                var bulkCopyArguments = bulkCopyAttribute.ConstructorArguments
                    .Select(a => a.Value)
                    .ToArray();
                
                if (bulkCopyArguments[0] is not INamedTypeSymbol targetEntityClassType)
                    continue;
                
                var (targetEntityNamespace, targetEntityName) = GetNamespaceAndClass(targetEntityClassType.ToDisplayString());
                var targetEntityClassData = new TypeMetadata
                {
                    TypeName = targetEntityName,
                    TypeNamespace = targetEntityNamespace
                }; 
                
                var (bulkCopyNamespace, bulkCopyName) = GetNamespaceAndClass(bulkCopyTypeDeclaration.ToDisplayString());
                var bulkCopyClassData = new TypeMetadata
                {
                    TypeName = bulkCopyName,
                    TypeNamespace = bulkCopyNamespace
                };

                var targetEntityProperties = targetEntityClassType.BaseType is null
                    ? targetEntityClassType.GetMembers() 
                    : targetEntityClassType.BaseType
                        .GetMembers()
                        .Concat(targetEntityClassType
                            .GetMembers());

                return new TypeToGenerate(
                    bulkCopyClassData, 
                    targetEntityClassData, 
                    targetEntityProperties
                        .Where(m => m.Kind == SymbolKind.Property 
                                    && m is
                                    {
                                        IsVirtual: false, 
                                        IsStatic: false, 
                                        IsAbstract: false, 
                                        CanBeReferencedByName: true
                                    })
                        .Select(s => s.Name)
                        .ToList());
                
            }
        }
        
        return null;
    }
    
    /// <summary>
    /// Получить имя и нэймспейс класса.
    /// </summary>
    /// <param name="classFullName"></param>
    /// <returns></returns>
    private static (string Namespace, string Class) GetNamespaceAndClass(string classFullName)
    {
        var separatorIndex = classFullName.LastIndexOf('.');
        return (classFullName.Substring(0, separatorIndex), classFullName.Substring(separatorIndex + 1));
    }
}