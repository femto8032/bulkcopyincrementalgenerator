namespace BulkCopy.SourceGeneration.Options;

internal class TypeMetadata
{
    internal string TypeName { get; set; }
    
    internal string TypeNamespace { get; set; }
}