namespace BulkCopy.SourceGeneration.Options;

internal struct TypeToGenerate
{
    internal readonly IReadOnlyList<string> TargetEntityProperties;
    
    internal readonly TypeMetadata BulkCopyClassData;
    internal readonly TypeMetadata TargetEntityClassData;

    internal TypeToGenerate(TypeMetadata bulkCopyClassData, TypeMetadata targetEntityClassData, IReadOnlyList<string> targetEntityProperties)
    {
        BulkCopyClassData = bulkCopyClassData;
        TargetEntityClassData = targetEntityClassData;
        TargetEntityProperties = targetEntityProperties;
    }
}