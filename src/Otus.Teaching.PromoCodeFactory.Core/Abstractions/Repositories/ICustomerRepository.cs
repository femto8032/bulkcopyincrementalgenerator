using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

public interface ICustomerRepository : IRepository<Customer>
{
    Task<Customer> CreateCustomerWithPreference(Customer entity, IReadOnlyList<CustomerPreference> customerPreferences, CancellationToken cancellationToken);
    
    Task<Customer> UpdateCustomerWithPreference(Customer entity, IEnumerable<CustomerPreference> customerPreferences, CancellationToken cancellationToken);
}