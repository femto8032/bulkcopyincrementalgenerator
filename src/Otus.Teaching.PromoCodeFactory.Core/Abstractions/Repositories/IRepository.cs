﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

public interface IRepository<T> where T : BaseEntity
{
    IQueryable<T> GetAll();
        
    IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);
        
    Task<T> CreateAsync(T entity, CancellationToken cancellationToken);

    Task<T> UpdateAsync(T entity, CancellationToken cancellationToken);
        
    Task<int> DeleteAsync(Expression<Func<T, bool>> expression, CancellationToken cancellationToken);
}