using BulkCopy;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.BulkCopy;

[BulkCopy(typeof(Customer))]
public sealed partial class CustomerBulkCopyModel { }