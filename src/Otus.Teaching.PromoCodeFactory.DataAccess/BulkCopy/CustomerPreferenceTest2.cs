using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BulkCopy;
using Npgsql;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.BulkCopy;

public sealed class CustomerPreferenceTest2
{
    private readonly NpgsqlConnection _npgsqlConnection;

    public CustomerPreferenceTest2(NpgsqlConnection npgsqlConnection)
    {
        _npgsqlConnection = npgsqlConnection;
    }

    public async Task CopyAsync(IReadOnlyList<CustomerPreference> source, TableToEntityMap tableToEntityMap, CancellationToken cancellationToken = default)
    {
        var tableColumns = string
            .Join(", ", tableToEntityMap.PropertyToColumnMap
             .Select(p => $@"""{p.ColumnName}"""));

        await using var writer = await _npgsqlConnection?.BeginBinaryImportAsync(
            $@"COPY ""{tableToEntityMap.TableName}"" ({tableColumns}) FROM STDIN (FORMAT BINARY)",
            cancellationToken)!;
        
        foreach (var item in source)
        {
            await writer.StartRowAsync(cancellationToken);
            foreach (var propertyName in tableToEntityMap.PropertyToColumnMap.Select(p => p.PropertyName))
            {
                await writer.WriteAsync(GetPropertyValue(propertyName, item), cancellationToken);
            }
        }

        await writer.CompleteAsync(cancellationToken);
    }

    private static object GetPropertyValue(string propertyName, CustomerPreference item)
    {
        return propertyName switch
        {
            "CustomerId" => item.CustomerId,
            "PreferenceId" => item.PreferenceId,
            _ => throw new ArgumentOutOfRangeException(nameof(propertyName))
        };
    }
}