using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Configurations;

internal sealed class CustomerConfiguration : IEntityTypeConfiguration<Customer>
{
    public void Configure(EntityTypeBuilder<Customer> builder)
    {
        builder.HasKey(p => p.Id);

        builder
            .Property(p => p.FirstName)
            .HasMaxLength(50)
            .IsRequired();
        
        builder
            .Property(p => p.LastName)
            .HasMaxLength(50)
            .IsRequired();

        builder.Ignore(p => p.FullName);

        builder.HasData(FakeDataFactory.Customers);
        
        builder
            .Property(p => p.Email)
            .HasMaxLength(50)
            .IsRequired();

        builder
            .HasMany(p => p.PromoCodes)
            .WithOne(p => p.Customer)
            .HasForeignKey(p => p.CustomerId);

        builder
            .HasMany(p => p.Preferences)
            .WithMany(p => p.Customers)
            .UsingEntity<CustomerPreference>(
                f => f
                    .HasOne(x => x.Preference)
                    .WithMany(y => y.CustomerPreferences)
                    .HasForeignKey(x => x.PreferenceId),
                f => f
                    .HasOne(x => x.Customer)
                    .WithMany(x => x.CustomerPreferences)
                    .HasForeignKey(x => x.CustomerId),
                f =>
                {
                    f.HasKey(k => new { k.CustomerId, k.PreferenceId });
                });

    }
}