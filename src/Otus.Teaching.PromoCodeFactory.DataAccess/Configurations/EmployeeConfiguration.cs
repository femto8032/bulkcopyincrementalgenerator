using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Configurations;

internal sealed class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
{
    public void Configure(EntityTypeBuilder<Employee> builder)
    {
        builder.HasKey(p => p.Id);

        builder.Ignore(p => p.FullName);

        builder.HasData(FakeDataFactory.Employees);

        builder
            .Property(p => p.FirstName)
            .HasMaxLength(50)
            .IsRequired();
        
        builder
            .Property(p => p.LastName)
            .HasMaxLength(50)
            .IsRequired();

        builder
            .Property(p => p.Email)
            .HasMaxLength(50)
            .IsRequired();
        
        builder
            .Property(p => p.AppliedPromocodesCount)
            .IsRequired();
        
        builder
            .HasOne(p => p.Role)
            .WithMany()
            .HasForeignKey(r => r.RoleId)
            .IsRequired();
    }
}