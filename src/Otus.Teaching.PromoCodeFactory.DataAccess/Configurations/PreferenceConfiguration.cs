using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Configurations;

internal sealed class PreferenceConfiguration : IEntityTypeConfiguration<Preference>
{
    public void Configure(EntityTypeBuilder<Preference> builder)
    {
        builder
            .HasKey(p => p.Id);
        
        builder
            .HasIndex(p => p.Name);
        
        builder
            .HasData(FakeDataFactory.Preferences);

        builder
            .Property(p => p.Name)
            .HasMaxLength(50)
            .IsRequired();
    }
}