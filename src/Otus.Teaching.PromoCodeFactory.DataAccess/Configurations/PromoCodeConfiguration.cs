using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Configurations;

internal sealed class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
{
    public void Configure(EntityTypeBuilder<PromoCode> builder)
    {
        builder.HasKey(p => p.Id);
        
        builder.HasData(FakeDataFactory.PromoCodes);
        
        builder
            .Property(p => p.Code)
            .HasMaxLength(50)
            .IsRequired();
        
        builder
            .Property(p => p.PartnerName)
            .HasMaxLength(50)
            .IsRequired();
        
        builder
            .Property(p => p.ServiceInfo)
            .HasMaxLength(500)
            .IsRequired();
        
        builder
            .HasOne(p => p.Preference)
            .WithMany()
            .HasForeignKey(p => p.PreferenceId);
        
        builder
            .HasOne(p => p.PartnerManager)
            .WithMany()
            .HasForeignKey(p => p.PartnerManagerId);
    }
}