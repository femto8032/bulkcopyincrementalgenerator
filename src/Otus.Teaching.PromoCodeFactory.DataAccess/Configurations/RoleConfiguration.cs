using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Configurations;

internal sealed class RoleConfiguration : IEntityTypeConfiguration<Role>
{
    public void Configure(EntityTypeBuilder<Role> builder)
    {
        builder.HasKey(p => p.Id);

        builder.HasData(FakeDataFactory.Roles);

        builder
            .Property(p => p.Name)
            .HasMaxLength(50)
            .IsRequired();
        
        builder
            .Property(p => p.Description)
            .HasMaxLength(400)
            .IsRequired();
    }
}