﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data;

public static class FakeDataFactory
{
    private static readonly Guid AdminId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02");
    private static readonly Guid PartnerManagerId = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665");
    private static readonly Guid Customer1Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
    private static readonly Guid Customer2Id = Guid.Parse("b9d4fb1a-5e44-4b7c-8875-fa54e9786d85");
    private static readonly Guid PromoCode1Id = Guid.Parse("c32a7fe3-ed5d-4d26-98b9-8401d020fce1");
    private static readonly Guid PromoCode2Id = Guid.Parse("b599de6f-fa93-4b99-a17c-21909227f1e7");
    private static readonly Guid PromoCode3Id = Guid.Parse("0735bd82-0f95-499f-956e-1b40aff4631b");
    private static readonly Guid Emploeey1Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f");
    private static readonly Guid Emploeey2Id = Guid.Parse("970ba868-98e8-4be0-b461-e8ed27b40f7a");
    private static readonly Guid TheatrePreferenceIdId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c");
    private static readonly Guid FamilyPreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd");
    private static readonly Guid ChildrenPreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84");

    public static IEnumerable<Employee> Employees => new List<Employee>
    {
        new()
        {
            Id = Emploeey1Id,
            Email = "owner@somemail.ru",
            FirstName = "Иван",
            LastName = "Сергеев",
            RoleId = AdminId,
            AppliedPromocodesCount = 5
        },
        new()
        {
            Id = Emploeey2Id,
            Email = "andreev@somemail.ru",
            FirstName = "Петр",
            LastName = "Андреев",
            RoleId = PartnerManagerId,
            AppliedPromocodesCount = 10
        },
    };

    public static IEnumerable<Role> Roles => new List<Role>
    {
        new()
        {
            Id = AdminId,
            Name = "Admin",
            Description = "Администратор",
        },
        new()
        {
            Id = PartnerManagerId,
            Name = "PartnerManager",
            Description = "Партнерский менеджер"
        }
    };
        
    public static IEnumerable<Preference> Preferences => new List<Preference>
    {
        new()
        {
            Id = TheatrePreferenceIdId,
            Name = "Театр",
        },
        new()
        {
            Id = FamilyPreferenceId,
            Name = "Семья",
        },
        new()
        {
            Id = ChildrenPreferenceId,
            Name = "Дети",
        }
    };
    
    public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>
    {
        new()
        {
            Id = PromoCode1Id,
            Code = "9DRrmW3O",
            BeginDate = DateTimeOffset
                .ParseExact("2023-06-24 14:40:52,531", "yyyy-MM-dd HH:mm:ss,fff", CultureInfo.InvariantCulture)
                .AddDays(7),
            EndDate = DateTimeOffset
                .ParseExact("2023-06-24 14:40:52,531", "yyyy-MM-dd HH:mm:ss,fff", CultureInfo.InvariantCulture)
                .AddDays(-7),
            PartnerName = "Otus",
            ServiceInfo = "Education service",
            CustomerId = Customer1Id,
            PartnerManagerId = Emploeey1Id,
            PreferenceId = TheatrePreferenceIdId
        },
        new()
        {
            Id = PromoCode2Id,
            Code = "loQLfCnk",
            BeginDate = DateTimeOffset
                .ParseExact("2023-06-24 14:40:52,531", "yyyy-MM-dd HH:mm:ss,fff", CultureInfo.InvariantCulture)
                .AddDays(7),
            EndDate = DateTimeOffset
                .ParseExact("2023-06-24 14:40:52,531", "yyyy-MM-dd HH:mm:ss,fff", CultureInfo.InvariantCulture)
                .AddDays(-7),
            PartnerName = "Big theatre",
            ServiceInfo = "Culture service",
            CustomerId = Customer1Id,
            PartnerManagerId = Emploeey2Id,
            PreferenceId = TheatrePreferenceIdId
        },
        new()
        {
            Id = PromoCode3Id,
            Code = "JKiMfDaL",
            BeginDate = DateTimeOffset
                .ParseExact("2023-06-24 14:40:52,531", "yyyy-MM-dd HH:mm:ss,fff", CultureInfo.InvariantCulture)
                .AddDays(7),
            EndDate = DateTimeOffset
                .ParseExact("2023-06-24 14:40:52,531", "yyyy-MM-dd HH:mm:ss,fff", CultureInfo.InvariantCulture)
                .AddDays(-7),
            PartnerName = "Child world",
            ServiceInfo = "Entertainment service",
            CustomerId = Customer2Id,
            PartnerManagerId = Emploeey2Id,
            PreferenceId = ChildrenPreferenceId
        }
    };

    public static IEnumerable<Customer> Customers
    {
        get
        {
            var customers = new List<Customer>
            {
                new()
                {
                    Id = Customer1Id,
                    Email = "ivan_sergeev@mail.ru",
                    FirstName = "Иван",
                    LastName = "Петров",
                },
                new()
                {
                Id = Customer2Id,
                Email = "aleksey_gavrilov@mail.ru",
                FirstName = "Алексей",
                LastName = "Гаврилов",
            }
            };

            return customers;
        }
    }
    
    public static IEnumerable<CustomerPreference> CustomerPreferences
    {
        get
        { 
            var customers = new List<CustomerPreference>
            {
                new()
                {
                    CustomerId = Customer1Id,
                    PreferenceId = TheatrePreferenceIdId
                },
                new()
                {
                    CustomerId = Customer1Id,
                    PreferenceId = FamilyPreferenceId
                },
                new()
                {
                    CustomerId = Customer2Id,
                    PreferenceId = ChildrenPreferenceId
                }
            };

            return customers;
        }
    }
}