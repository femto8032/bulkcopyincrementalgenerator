using System.Linq;
using BulkCopy;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;

public static class EntityExtensions
{
    public static TableToEntityMap GetTableMapping(this IEntityType entityType)
    {
        return new TableToEntityMap
        {
            TableName = entityType.GetTableName(),
            PropertyToColumnMap = entityType
                .GetProperties()
                .Select(p => new PropertyToColumnMap
                {
                    PropertyName = p.Name,
                    ColumnName = p.GetColumnName()
                }).ToList()
        };
    }
}