using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Npgsql;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.BulkCopy;
using Otus.Teaching.PromoCodeFactory.DataAccess.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public sealed class CustomerRepository : EfRepository<Customer>, ICustomerRepository
{
    private readonly ILogger<CustomerRepository> _logger;
    
    public CustomerRepository(PromoCodeFactoryDbContext context, ILogger<CustomerRepository> logger) : base(context)
    {
        _logger = logger;
    }

    public async Task<Customer> CreateCustomerWithPreference(Customer entity, IReadOnlyList<CustomerPreference> customerPreferences, CancellationToken cancellationToken)
    {
        await using var transaction = await Context.Database.BeginTransactionAsync(IsolationLevel.ReadCommitted, cancellationToken);
        var connection = Context.Database.GetDbConnection() as NpgsqlConnection;

        try
        {
            var createdCustomer = Context.Customers.Add(entity).Entity;
            await Context.SaveChangesAsync(cancellationToken: cancellationToken);
            
            var entityType = Context.Model.FindEntityType(typeof(CustomerPreference)).GetTableMapping();
            var bulkCopyModel = new CustomerPreferenceBulkCopyModel(connection);
            await bulkCopyModel.CopyAsync(customerPreferences, entityType, cancellationToken);

            await transaction.CommitAsync(cancellationToken);

            return createdCustomer;
        }
        catch (Exception e)
        {
            _logger.Log(LogLevel.Error, e.Message);

            await transaction.RollbackAsync(cancellationToken);
            throw new DatabaseException("Cannot create customer.");
        }
        finally
        {
            await connection?.CloseAsync()!;
        }
    }

    public async Task<Customer> UpdateCustomerWithPreference(Customer entity, IEnumerable<CustomerPreference> customerPreferences, CancellationToken cancellationToken)
    {
        await using var transaction = await Context.Database.BeginTransactionAsync(IsolationLevel.ReadCommitted, cancellationToken);
        
        try
        {
            await Context.Customers
                .Where(c => c.Id == entity.Id)
                .ExecuteUpdateAsync(s => s
                    .SetProperty(c => c.Email, c => entity.Email)
                    .SetProperty(c => c.FirstName, c => entity.FirstName)
                    .SetProperty(c => c.LastName, c => entity.LastName), 
                    cancellationToken: cancellationToken);

            await Context.CustomerPreferences
                .Where(c => c.CustomerId == entity.Id)
                .ExecuteDeleteAsync(cancellationToken);
            
            await Context.BulkInsertAsync(customerPreferences, cancellationToken: cancellationToken);
        
            await transaction.CommitAsync(cancellationToken);

            return entity;
        }
        catch (Exception e)
        {
            _logger.Log(LogLevel.Error, e.Message);
            
            await transaction.RollbackAsync(cancellationToken);
            throw new DatabaseException($"Cannot update customer with id {entity.Id}.");
        }
    }
}