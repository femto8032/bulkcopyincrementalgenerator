﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EfRepository<T> : IRepository<T> where T : BaseEntity
{
    protected readonly PromoCodeFactoryDbContext Context;

    public EfRepository(PromoCodeFactoryDbContext context)
    {
        Context = context;
    }

    IQueryable<T> IRepository<T>.GetAll()
    {
        return Context.Set<T>().AsNoTracking();
    }

    public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
    {
        return Context.Set<T>().Where(expression);
    }

    public async Task<T> CreateAsync(T entity, CancellationToken cancellationToken)
    {
        var response = (await Context.Set<T>().AddAsync(entity, cancellationToken)).Entity;

        await Context.SaveChangesAsync(cancellationToken);

        return response;
    }

    public async Task<T> UpdateAsync(T entity, CancellationToken cancellationToken)
    {
        var result = Context.Set<T>().Update(entity).Entity;

        await Context.SaveChangesAsync(cancellationToken: cancellationToken);

        return result;
    }

    public async Task<int> DeleteAsync(Expression<Func<T, bool>> expression, CancellationToken cancellationToken)
    {
        return await Context
            .Set<T>()
            .Where(expression)
            .ExecuteDeleteAsync(cancellationToken: cancellationToken);
    }
}