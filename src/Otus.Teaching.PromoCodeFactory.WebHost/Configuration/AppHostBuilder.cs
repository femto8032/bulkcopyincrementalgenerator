using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Mapping;
using Otus.Teaching.PromoCodeFactory.WebHost.Options;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Configuration;

public static class AppHostBuilder
{
    public static WebApplication Build(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddControllers();
        builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
        builder.Services.AddScoped<ICustomerRepository, CustomerRepository>();
        builder.Services.RegisterMapsterConfiguration();

        builder.Services
            .AddOptions<DatabaseOptions>()
            .BindConfiguration(nameof(DatabaseOptions))
            .ValidateDataAnnotations()
            .ValidateOnStart();
        
        builder.Services.AddDbContext<PromoCodeFactoryDbContext>(
            (serviceProvider, optionsBuilder) =>
            {
                var serviceOptions = serviceProvider.GetRequiredService<IOptions<DatabaseOptions>>().Value;

                optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

                optionsBuilder.UseNpgsql(
                    serviceOptions.ConnectionString,
                    options =>
                    {
                        options.CommandTimeout(serviceOptions.CommandTimeout);
                    });
            });
        
        builder.Services.AddHostedService<InitializeDatabaseService>();

        builder.Services.ConfigureOptions<SwaggerVersioningOption>();
        builder.Services.ConfigureOptions<SwaggerUiVersioningOption>();
        
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddVersionedApiExplorer(o =>
        {
            o.GroupNameFormat = "'v'VVV";
            o.SubstituteApiVersionInUrl = true;
        });

        builder.Services.AddApiVersioning(o =>
        {
            o.DefaultApiVersion = new ApiVersion(1, 0);
            o.AssumeDefaultVersionWhenUnspecified = true;
            o.ReportApiVersions = true;
        });

        builder.Services.AddSwaggerGen();
        
        return builder.Build();
    }
}