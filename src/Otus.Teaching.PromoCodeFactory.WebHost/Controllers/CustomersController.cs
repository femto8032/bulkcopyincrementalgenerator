﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Клиенты
/// </summary>
[ApiController]
[ApiVersion("1.0")]
[Produces("application/json")]
[Route($"api/v{{v:apiVersion}}/[controller]")]
public sealed class CustomersController : ControllerBase
{
    private readonly ICustomerRepository _customers;

    public CustomersController(ICustomerRepository customers)
    {
        _customers = customers;
    }

    /// <summary>
    /// Получить список клиентов.
    /// </summary>
    /// <returns>DTO клиентов.</returns>
    [HttpGet]
    public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
    {
        var customers = await _customers
            .GetAll()
            .AsNoTracking()
            .ProjectToType<CustomerShortResponse>()
            .ToListAsync(HttpContext.RequestAborted);
        
        return Ok(customers);
    }
        
    /// <summary>
    /// Получить клиента по идентификатору со списком купонов и предпочтений.
    /// </summary>
    /// <param name="id">Идентификатор клиента.</param>
    /// <returns>DTO клиента.</returns>
    [HttpGet("{id:guid:required}")]
    public async Task<ActionResult<CustomerResponse>> GetCustomer([FromRoute] Guid id)
    {
        var customer = await _customers
            .FindByCondition(c => c.Id == id)
            .AsNoTracking()
            .Include(p => p.Preferences)
            .Include(p => p.PromoCodes)
            .ProjectToType<CustomerResponse>()
            .AsSplitQuery()
            .SingleOrDefaultAsync(HttpContext.RequestAborted);
        
        if(customer is null)
            throw new NotFoundException($"Client with id {id} not found");
        
        return Ok(customer);
    }
    
    /// <summary>
    /// Добавить клиента со списком предпочтений.
    /// </summary>
    /// <param name="request">DTO с данными о клиенте и списком предпочтений.</param>
    /// <returns>Соззданный клиент.</returns>
    [HttpPost]
    public async Task<ActionResult<CreateOrEditCustomerResponse>> CreateCustomer([FromBody] CreateCustomerRequest request)
    {
        var adaptedCustomer = request.Adapt<Customer>();
        var addedPreferences = request.PreferenceIds.Select(p => new CustomerPreference
        {
            CustomerId = adaptedCustomer.Id,
            PreferenceId = p
        });
        
        var createdCustomer = await _customers.CreateCustomerWithPreference(adaptedCustomer, addedPreferences.ToList(), HttpContext.RequestAborted);
        
        var createdCustomerResponse = createdCustomer.Adapt<CreateOrEditCustomerResponse>();
        createdCustomerResponse.PreferenceIds = request.PreferenceIds;
        
        return CreatedAtAction(nameof(GetCustomer),new { id = createdCustomer.Id }, createdCustomerResponse);
    }
    
    /// <summary>
    /// Обновить клиента и его список предпочтений.
    /// </summary>
    /// <param name="id">Идентификатор клиента.</param>
    /// <param name="request">DTO с данными о клиенте и списком предпочтений.</param>
    /// <returns>Обновлённый клиент.</returns>
    [HttpPut("{id:guid:required}")]
    public async Task<ActionResult> EditCustomersAsync([FromRoute] Guid id, [FromBody] EditCustomerRequest request)
    {
        var completeCustomerModel = request with { Id = id };
        
        var isCustomerExist = await _customers
            .FindByCondition(c => c.Id == completeCustomerModel.Id)
            .AnyAsync(HttpContext.RequestAborted);

        if (!isCustomerExist)
            throw new NotFoundException($"Client with id {id} not found");
        
        var updatedPreferences = completeCustomerModel.PreferenceIds.Select(p => new CustomerPreference
        {
            CustomerId = completeCustomerModel.Id,
            PreferenceId = p
        });
        
        var updatedCustomer = await _customers
            .UpdateCustomerWithPreference(completeCustomerModel.Adapt<Customer>(), updatedPreferences, HttpContext.RequestAborted);
        
        var updatedCustomerResponse = updatedCustomer.Adapt<CreateOrEditCustomerResponse>();
        updatedCustomerResponse.PreferenceIds = completeCustomerModel.PreferenceIds;
        
        return Ok(updatedCustomerResponse);
    }
    
    /// <summary>
    /// Удалить клиента по идентификатору.
    /// </summary>
    /// <param name="id">Идентификатор клиента.</param>
    [HttpDelete("{id:guid:required}")]
    public async Task<IActionResult> DeleteCustomer([FromRoute] Guid id)
    {
        var isCustomerExist = await _customers
            .FindByCondition(c => c.Id == id)
            .AnyAsync(HttpContext.RequestAborted);

        if (!isCustomerExist)
            throw new NotFoundException($"Client with id {id} not found");
        
        int deletedRows = await _customers.DeleteAsync(c => c.Id == id, HttpContext.RequestAborted);
        
        return deletedRows == 0 
            ? StatusCode((int)HttpStatusCode.InternalServerError) 
            : NoContent();
    }
}