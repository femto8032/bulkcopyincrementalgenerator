using System;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Preference;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Предпочтения.
/// </summary>
[ApiController]
[ApiVersion("1.0")]
[Produces("application/json")]
[Route($"api/v{{v:apiVersion}}/[controller]")]
public sealed class PreferencesController : ControllerBase
{
    private readonly IRepository<Preference> _preferences;

    public PreferencesController(IRepository<Preference> preferences)
    {
        _preferences = preferences;
    }

    /// <summary>
    /// Получить список предпочтений.
    /// </summary>
    /// <returns>DTO предпочтений.</returns>
    [HttpGet]
    public async Task<ActionResult<PreferenceShortResponse>> GetPreferences()
    {
        var preferences = await _preferences
            .GetAll()
            .AsNoTracking()
            .ProjectToType<PreferenceShortResponse>()
            .ToListAsync(HttpContext.RequestAborted);
        
        return Ok(preferences);
    }
        
    /// <summary>
    /// Получить предпочтение по идентификатору со списком клиентов.
    /// </summary>
    /// <param name="id">Идентификатор предпочтения.</param>
    /// <returns>DTO предпочтения.</returns>
    [HttpGet("{id:guid:required}")]
    public async Task<ActionResult<PreferenceResponse>> GetPreference([FromRoute] Guid id)
    {
        var preference = await _preferences
            .FindByCondition(c => c.Id == id)
            .AsNoTracking()
            .Include(p => p.Customers)
            .ProjectToType<PreferenceResponse>()
            .AsSplitQuery()
            .SingleOrDefaultAsync(HttpContext.RequestAborted);
        
        if(preference is null)
            throw new NotFoundException($"Preference with id {id} not found");
        
        return Ok(preference);
    }
}