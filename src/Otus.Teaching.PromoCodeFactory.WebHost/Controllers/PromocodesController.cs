﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.PromoCode;
using Guid = System.Guid;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Промокоды.
/// </summary>
[ApiController]
[ApiVersion("1.0")]
[Produces("application/json")]
[Route($"api/v{{v:apiVersion}}/[controller]")]
public class PromocodeController : ControllerBase
{
    private readonly IRepository<PromoCode> _promoCodes;
    private readonly IRepository<Employee> _employees;
    private readonly IRepository<Preference> _preferences;
    private readonly IRepository<Customer> _customers;

    public PromocodeController(
        IRepository<PromoCode> promoCodes, 
        IRepository<Employee> employees, 
        IRepository<Preference> preferences, 
        IRepository<Customer> customers)
    {
        _promoCodes = promoCodes;
        _employees = employees;
        _preferences = preferences;
        _customers = customers;
    }

    /// <summary>
    /// Получить все промокоды.
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
    {
        var promoCodes = await _promoCodes
            .GetAll()
            .AsNoTracking()
            .ProjectToType<PromoCodeShortResponse>()
            .ToListAsync(HttpContext.RequestAborted);
        
        return Ok(promoCodes);
    }
    
    /// <summary>
    /// Получить промокод по идентификатору.
    /// </summary>
    /// <param name="id">Идентификатор промокода.</param>
    /// <returns>DTO промокода.</returns>
    [HttpGet("{id:guid:required}")]
    public async Task<ActionResult<PromoCodeShortResponse>> GetPromoCode([FromRoute] Guid id)
    {
        var promoCode = await _promoCodes
            .FindByCondition(c => c.Id == id)
            .AsNoTracking()
            .ProjectToType<PromoCodeShortResponse>()
            .AsSplitQuery()
            .SingleOrDefaultAsync(HttpContext.RequestAborted);
        
        if(promoCode is null)
            throw new NotFoundException($"Promo code with id {id} not found");
        
        return Ok(promoCode);
    }
        
    /// <summary>
    /// Создать промокод и выдать его клиентам с указанным предпочтением.
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<PromoCodeShortResponse>> GivePromoCodesToCustomersWithPreference([FromBody] GivePromoCodeRequest request)
    {
        Guid preferenceId = await _preferences
            .FindByCondition(p => p.Name == request.Preference)
            .Select(p => p.Id)
            .FirstOrDefaultAsync(HttpContext.RequestAborted);

        if (preferenceId == Guid.Empty)
            throw new NotFoundException($"Preference with name {request.Preference} not found.");
        
        bool isPartnerExist = await _employees
            .FindByCondition(p => p.Id == request.PartnerManagerId)
            .AnyAsync(HttpContext.RequestAborted);
        
        if (!isPartnerExist)
            throw new NotFoundException($"Partner manager with id {request.PartnerManagerId} not found.");

        Guid randomCustomerId = await _customers
            .GetAll()
            .SelectMany(c => c.CustomerPreferences)
            .Where(c => c.PreferenceId == preferenceId)
            .Select(c => c.CustomerId)
            .FirstOrDefaultAsync(HttpContext.RequestAborted);
        
        if (randomCustomerId == Guid.Empty)
            throw new NotFoundException("Nobody customer found.");

        var promoCode = request.Adapt<PromoCode>();
        promoCode.EndDate = promoCode.BeginDate.AddDays(14);
        promoCode.CustomerId = randomCustomerId;
        promoCode.PreferenceId = preferenceId;

        var createdPromoCode = await _promoCodes.CreateAsync(promoCode, HttpContext.RequestAborted);

        return CreatedAtAction(
            nameof(GetPromoCode),
            new { id = createdPromoCode.Id }, 
            createdPromoCode.Adapt<PromoCodeShortResponse>());
    }
}