using System;
using Mapster;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.PromoCode;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping;

public static class MapsterConfig
{
    public static IServiceCollection RegisterMapsterConfiguration(this IServiceCollection services)
    {
        TypeAdapterConfig<GivePromoCodeRequest, PromoCode>
            .NewConfig()
            .Ignore(p => p.Customer)
            .Ignore(p => p.Preference)
            .Ignore(p => p.PartnerManager)
            .Ignore(p => p.EndDate)
            .Map(dest => dest.Code, src => src.PromoCode)
            .Map(dest => dest.BeginDate, src => DateTimeOffset.Now);
        
        return services;
    }
}