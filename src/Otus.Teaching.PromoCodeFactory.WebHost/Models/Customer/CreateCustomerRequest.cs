﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;

public sealed record CreateCustomerRequest
{
    [Required]
    public string FirstName { get; init; }
    
    [Required]
    public string LastName { get; init; }
    
    [Required]
    [EmailAddress]
    public string Email { get; init; }
    
    [Required]
    public List<Guid> PreferenceIds { get; init; }
}