using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;

public sealed record CreateOrEditCustomerResponse
{
    public string Id { get; init; }
    public string FirstName { get; init; }
    public string LastName { get; init; }
    public string Email { get; init; }
    
    public IReadOnlyCollection<Guid> PreferenceIds { get; set; }
}