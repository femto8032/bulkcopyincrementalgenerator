﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Preference;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.PromoCode;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;

public sealed record CustomerResponse
{
    public Guid Id { get; init; }
    public string FirstName { get; init; }
    public string LastName { get; init; }
    public string Email { get; init; }
        
    public IReadOnlyCollection<PreferenceShortResponse> Preferences { get; init; }
    public IReadOnlyCollection<PromoCodeShortResponse> PromoCodes { get; init; }
}