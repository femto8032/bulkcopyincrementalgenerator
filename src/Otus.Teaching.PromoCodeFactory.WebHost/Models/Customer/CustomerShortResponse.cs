﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;

public sealed record CustomerShortResponse
{
    public Guid Id { get; init; }
    public string FirstName { get; init; }
    public string LastName { get; init; }
    public string Email { get; init; }
}