using System.Text.Json;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models;

public sealed record ErrorDetails
{
    public int StatusCode { get; init; }
    
    public string Message { get; init; }
    
    public override string ToString()
    {
        return JsonSerializer.Serialize(this);
    }
}