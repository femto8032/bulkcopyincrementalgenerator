namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Preference;

public sealed record PreferenceShortResponse
{
    public string Name { get; init; }
}