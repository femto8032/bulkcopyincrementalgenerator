﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.PromoCode;

public sealed class GivePromoCodeRequest
{
    [Required]
    public string ServiceInfo { get; init; }

    [Required]
    public string PartnerName { get; init; }

    [Required]
    public string PromoCode { get; init; }

    [Required]
    public string Preference { get; init; }
    
    [Required]
    public Guid PartnerManagerId { get; init; }
}