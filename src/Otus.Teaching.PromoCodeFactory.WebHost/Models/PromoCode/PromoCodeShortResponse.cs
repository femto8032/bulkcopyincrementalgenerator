﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.PromoCode;

public sealed record PromoCodeShortResponse
{
    public Guid Id { get; init; }
        
    public string Code { get; init; }

    public string ServiceInfo { get; init; }

    public string BeginDate { get; init; }

    public string EndDate { get; init; }

    public string PartnerName { get; init; }
}