﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Role;

public sealed record RoleItemResponse
{
    public Guid Id { get; init; }
    
    public string Name { get; init; }

    public string Description { get; init; }
}