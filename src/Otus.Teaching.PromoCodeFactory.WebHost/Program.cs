using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.WebHost.Configuration;
using Otus.Teaching.PromoCodeFactory.WebHost.Middleware;

Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
Trace.AutoFlush = true;
Trace.Indent();

try
{
    Trace.WriteLine("Starting host...");
    
    TaskScheduler.UnobservedTaskException += (_, e) => Trace.WriteLine(e.Exception, "Unhandled exception was thrown");

    var app = AppHostBuilder.Build(args);
    
    if (app.Environment.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
        app.UseSwagger();
        app.UseSwaggerUI();
    }
    else
    {
        app.UseHsts();
    }

    app.UseMiddleware<ExceptionMiddleware>();
    app.UseHttpsRedirection();
    app.UseRouting();
    app.MapControllers();

    await app.RunAsync();
    return 0;
}
catch (Exception e)
{
    Trace.WriteLine(e, "An unhandled exception occured starting application.");
    return 1;
}