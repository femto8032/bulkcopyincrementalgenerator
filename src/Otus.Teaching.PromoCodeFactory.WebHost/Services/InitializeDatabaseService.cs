using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services;

internal sealed class InitializeDatabaseService : IHostedService
{
    private readonly IServiceProvider _serviceProvider;

    public InitializeDatabaseService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }
    
    public async Task StartAsync(CancellationToken cancellationToken)
    {
        using var scope = _serviceProvider.CreateScope();
        
        var dbContext = scope.ServiceProvider.GetRequiredService<PromoCodeFactoryDbContext>();
        
        await dbContext.Database.MigrateAsync(cancellationToken: cancellationToken);
        
        bool isDatabaseAvailable = await dbContext.Database.CanConnectAsync(cancellationToken);
        if (!isDatabaseAvailable)
            throw new DatabaseException("Database is not connected.");
    }

    public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
}